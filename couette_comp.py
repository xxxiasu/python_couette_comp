#===========================================================================================
#=                                                                                        ==
#=    Programme pour tracer les courbes du TD 7 : ecoulement de Couette compressible      ==
#=                                                                                        ==
#=                                                                        par Xiasu YANG  ==
#=                                                                            01/04/2019  ==
#===========================================================================================

#-importer les bibliotheques necessaires---------------------------------------------------
import numpy as np                                                                         #
import matplotlib.pyplot as plt                                                            #
#------------------------------------------------------------------------------------------

#-definir les courbes a tracer-------------------------------------------------------------
def fct_y_h(u_ue, Me):    # courbe y/h = f(u/ue), notons que Me est aussi une variable     #
    y_h = (u_ue + Pr*(gamma-1)/2*Me**2*(u_ue - 1/3*u_ue**3))/(1 + Pr*(gamma-1)/3*Me**2)    #
    return y_h                                                                             #
vfct_y_h   = np.vectorize(fct_y_h)   # vectorisation de la fonction y/h                    #
                                                                                           #
def fct_Tw_Te(Me):        # courbe Tw/Te = f(Me)                                           #
    Tw_Te = 1 + Pr/2*(gamma - 1)*Me**2                                                     #
    return Tw_Te                                                                           #
vfct_Tw_Te = np.vectorize(fct_Tw_Te) # vectorisation de la fonction Tw/Te                  #
                                                                                           #
#====================================BONUS !!!=============================================#
def fct_T_Te(u_ue, Me):   # fonction T/Te = f(u/ue), pour tracer le profile de T/Te        #
    T_Te = 1 + Pr*(gamma-1)/2*Me**2*(1 - u_ue**2)                                          #
    return T_Te                                                                            #
vfct_T_Te  = np.vectorize(fct_T_Te)  # vectorisation de la fonction T/Te                   #
#------------------------------------------------------------------------------------------

#-definir les constantes-------------------------------------------------------------------
# constantes physiques:                                                                    #
gamma      = 1.4   # Rapport des capacites thermiques Cp/Cv                                #
Pr         = 0.66  # Nombre de Prandtl                                                     #
# parametres des courbes: (a vous de jouer !!!)                                            #
N_pnts     = 100   # Nombre de points a tracer, l'augmenter pour des courbes plus lisses   #
N_crbs     = 6     # Nombre de courbes u/ue = f(y/h) a tracer, modifiable                  #
Me_pas     = 1.0   # Pas de Me entre 2 courbes u/ue = f(y/h), modifiable                   #
Me_max     = 5     # Me maximal pour la courbe Tw/Te = f(Me), modifiable                   #
#------------------------------------------------------------------------------------------

#-definir les variables independantes------------------------------------------------------
u_ue_tab   = np.arange(N_pnts) * (  1   /(N_pnts-1)) # u/ue varie de 0 a 1                 #
Me_tab     = np.arange(N_pnts) * (Me_max/(N_pnts-1)) #  Me  varie de 0 a Me_max defini     #
#------------------------------------------------------------------------------------------

#-tracer y/h = f(u/ue) avec N_crbs nombres de Mach-----------------------------------------
plt.figure(figsize=(12,9))                                                                 #
for i in range (N_crbs):                                    #vvvvvvvvvvvvvvvvvvvvvvvvvvv   #
    Me = i*Me_pas                                           # calculer y/h             |   #
    y_h_tab = vfct_y_h(u_ue_tab, Me)                        # et tracer u/ue=f(y/h)    |   #
    plt.plot(y_h_tab, u_ue_tab, label='Me = %.1f' %Me)      #^^^^^^^^^^^^^^^^^^^^^^^^^^^   #
plt.xlim(0.0, 1.0)                                                                         #
plt.ylim(0.0, 1.0)                                                                         #
plt.xlabel('y/h')                                                                          #
plt.ylabel('u/ue')                                                                         #
plt.legend()                                                                               #
plt.title('u/ue = f(y/h)')                                                                 #
plt.grid(True)                                                                             #
plt.show                                                                                   #
plt.savefig('courbe_1.png')                                                                #
#------------------------------------------------------------------------------------------

#-tracer Tw/Te = f(Me)---------------------------------------------------------------------
plt.figure(figsize=(12,9))                                                                 #
Tw_Te_tab  = vfct_Tw_Te(Me_tab)                             # calculer Tw_Te               #
plt.plot(Me_tab, Tw_Te_tab)                                                                #
plt.xlim(0.0, np.amax(Me_tab))                                                             #
plt.ylim(0.0, np.amax(Tw_Te_tab))                                                          #
plt.xlabel('Me')                                                                           #
plt.ylabel('Tw/Te')                                                                        #
plt.title('Tw/Te = f(Me)')                                                                 #
plt.grid(True)                                                                             #
plt.show                                                                                   #
plt.savefig('courbe_2.png')                                                                #
#------------------------------------------------------------------------------------------

#====================================BONUS !!!=================================================
#-tracer T/Te = f(y/h)---------------------------------------------------------------------    #
plt.figure(figsize=(12,9))                                                                 #   #
for i in range (N_crbs):                                    #vvvvvvvvvvvvvvvvvvvvvvvvvvv   #   #
    Me = i*Me_pas                                           # calculer y/h et T/Te     |   #   #
    y_h_tab  = vfct_y_h(u_ue_tab, Me)                       # et tracer T/Te=f(y/h)    |   #   #
    T_Te_tab = vfct_T_Te(u_ue_tab, Me)                      #                          |   #   #
    plt.plot(y_h_tab, T_Te_tab, label='Me = %.1f' %Me)      #^^^^^^^^^^^^^^^^^^^^^^^^^^^   #   #
plt.xlim(0.0, 1.0)                                                                         #   #
plt.ylim(0.0, np.amax(T_Te_tab))                                                           #   #
plt.xlabel('y/h')                                                                          #   #
plt.ylabel('T/Te')                                                                         #   #
plt.legend()                                                                               #   #
plt.title('T_Te = f(y/h)')                                                                 #   #
plt.grid(True)                                                                             #   #
plt.show                                                                                   #   #
plt.savefig('courbe_3.png')                                                                #   #
#------------------------------------------------------------------------------------------    #
                                                                                               #
#-tracer rho/rhoe = f(y/h) = Te/T----------------------------------------------------------    #
plt.figure(figsize=(12,9))                                                                 #   #
for i in range (N_crbs):                                    #vvvvvvvvvvvvvvvvvvvvvvvvvvv   #   #
    Me = i*Me_pas                                           # calculer y/h et rho/rhoe |   #   #
    y_h_tab  = vfct_y_h(u_ue_tab, Me)                       # et tracer rho/rhoe=f(y/h)|   #   #
    rho_rhoe_tab = 1/vfct_T_Te(u_ue_tab, Me)                #                          |   #   #
    plt.plot(y_h_tab, rho_rhoe_tab, label='Me = %.1f' %Me)  #^^^^^^^^^^^^^^^^^^^^^^^^^^^   #   #
plt.xlim(0.0, 1.0)                                                                         #   #
plt.ylim(0.0, 1.1)                                                                         #   #
plt.xlabel('y/h')                                                                          #   #
plt.ylabel('rho/rhoe')                                                                     #   #
plt.legend()                                                                               #   #
plt.title('rho_rhoe = f(y/h)')                                                             #   #
plt.grid(True)                                                                             #   #
plt.show                                                                                   #   #
plt.savefig('courbe_4.png')                                                                #   #
#------------------------------------------------------------------------------------------    #
#==============================================================================================
